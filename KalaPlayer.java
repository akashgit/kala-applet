/**
 * An abstract class representing a player in Kala.  Extend this class
 * to make your own players (e.g. human players entering moves at the keyboard
 * or computer players with programmed strategies for making moves).
 */
public abstract class KalaPlayer
{
    /**
     * Method by which a player selects a move.
     * @param gs The current game state
     * @return A side pit number in the range 1-6
     * @throws NoMoveAvailableException if all side pits for the player are empty 
     * (i.e. the game is over)
     */
    public abstract int chooseMove(KalaGameState gs) throws NoMoveAvailableException;
}
