import javax.swing.JOptionPane;

/**
 * This class represents the current state of a Kala game, including 
 * which player's turn it is along with the state of the board; i.e. the 
 * numbers of stones in each side pit, and each player's 'kala').
 *
 * Modified by Vaclav Hudec, Akash Srivastava
 */
public class KalaGameState implements Cloneable
{
	//in default Player A starts the game
    private int turnID;
    //two players, each having kala at index 0 and side pits at corresponding indexes
    private int[][] playerStates;
    
    //constants
    private final int NUM_SIDE_PITS = 6;
    private final int NUM_PLAYERS = 2;
    
    /**
     * Constructs a new GameState with a specified number of stones in each 
     * player's side pits.
     * @param startingStones the number of starting stones in each side pit.
     * @throws InvalidStartingStonesException if startingStones not in the range 1-10.
     */
    public KalaGameState(int startingStones) throws InvalidStartingStonesException
    {
        turnID = 0;
        playerStates = new int[NUM_PLAYERS][NUM_SIDE_PITS + 1]; // 1 stands for kala on index 0
        
        if(startingStones <= 0 || startingStones > 10)
        	throw new InvalidStartingStonesException(startingStones);
        
        //fill both players' side pits
        for(int[] player : playerStates)
        	for(int pit = 1; pit <= NUM_SIDE_PITS; pit++)
    			player[pit] = startingStones;
        
        playerStates[0][0] = playerStates[1][0] = 0; 
    }
    
    /**
     * Returns the ID of the player whose turn it is.  
     * @return A value of 0 = Player A, 1 = Player B.
     */
    public int getTurn()
    {
        return turnID;
    }
    
    private void changeTurn()
    {
    	if(turnID == 0)
    		turnID = 1;
    	else
    		turnID = 0;
    }
    
    private int secondPlayer()
    {
    	return secondPlayer(turnID);
    }
    
    private int secondPlayer(int currentPlayer)
    {
    	return currentPlayer == 1 ? 0 : 1;
    }
        
    /**
     * Returns the current kala for a specified player.
     * @param playerNum A value of 0 for Player A, 1 for Player B.
     * @throws IllegalPlayerNumException if the playerNum parameter 
     * is not 0 or 1.  
     */     
    public int getKala(int playerNum) throws IllegalPlayerNumException
    {
    	if(playerNum < 0 || playerNum >= NUM_PLAYERS)
    		throw new IllegalPlayerNumException(playerNum);
    	
        return playerStates[playerNum][0];
    }
    
    /**
     * Returns the current number of stones in the specified pit for 
     * the player whose turn it is.
     * @param sidePitNum the side pit being queried in the range 1-6.
     * @throws IllegalSidePitNumException if the sidePitNum parameter.
     * is not in the range 1-6.
     */ 
    public int getNumStones(int sidePitNum) throws IllegalSidePitNumException
    {
    	if(sidePitNum <= 0 || sidePitNum > NUM_SIDE_PITS)
    		throw new IllegalSidePitNumException(sidePitNum);
    	
        return playerStates[turnID][sidePitNum];
    }

    /**
     * Returns the current number of stones in the specified pit for a specified player.
     * @param playerNum the player whose kala is sought. (0 = Player A, 1 = Player B).  
     * @param sidePitNum the side pit being queried (in the range 1-6).
     * @throws IllegalPlayerNumException if the playerNum parameter is not 0 or 1.  
     * @throws IllegalSidePitNumException if the sidePitNum parameter is not in the 
     * range 1-6.
     */
    public int getNumStones(int playerNum, int sidePitNum) throws IllegalPlayerNumException, 
																  IllegalSidePitNumException
    {
    	if(playerNum < 0 || playerNum >= NUM_PLAYERS)
    		throw new IllegalPlayerNumException(playerNum);
    	
    	if(sidePitNum <= 0 || sidePitNum > NUM_SIDE_PITS)
    		throw new IllegalSidePitNumException(sidePitNum);
    	
        return playerStates[playerNum][sidePitNum];
    }
    
    /**
     * Returns the current score for a specified player - the player's 
     * kala plus the number of stones in each of their side pits.
     * @param playerNum the player whose kala is sought. (0 = Player A, 1 = Player B).  
     * @throws IllegalPlayerNumException if the playerNum parameter is not 0 or 1.  
     */ 
    public int getScore(int playerNum) throws IllegalPlayerNumException
    {
    	if(playerNum < 0 || playerNum >= NUM_PLAYERS)
    		throw new IllegalPlayerNumException(playerNum);
    	
    	int score = 0;
        for(int i = 0; i <= NUM_SIDE_PITS; i++)
			score += playerStates[playerNum][i];
       
        return score;
    }
    
    // I never use this method as I put kala at the 0th index of all pits array (1 kala + 6 pits)
    private int getSidePitArrayIndex(int sidePitNum) throws IllegalSidePitNumException
    {
    	if(sidePitNum <= 0 || sidePitNum > NUM_SIDE_PITS)
    		throw new IllegalSidePitNumException(sidePitNum);
    	
        return sidePitNum - 1;
    }
    
    public boolean gameOver()
    {	
    	//if there is at least one pit with more than zero stones
    	//in any of current player's side pits, the game is NOT over
    	for(int pit = 1; pit <= NUM_SIDE_PITS; pit++)
    	{
			if(playerStates[turnID][pit] > 0) return false;
    	}
        
    	//otherwise, it IS over
        return true;
    }

    /**
     * Makes a move for the player whose turn it is.
     * @param sidePitNum the side pit being queried (should be in the range 1-6)
     * @throws IllegalSidePitNumException if the sidePitNum parameter is not in the range 1-6.
     * @throws IllegalMoveException if the side pit is empty and has no stones in it.
     */ 
    public void makeMove(int sidePitNum) throws IllegalSidePitNumException, IllegalMoveException
    {
    	if(sidePitNum <= 0 || sidePitNum > NUM_SIDE_PITS)
    		throw new IllegalSidePitNumException(sidePitNum);
    	
    	if(playerStates[turnID][sidePitNum] == 0)
    		throw new IllegalMoveException(sidePitNum);
    	
    	//player over whose pits stones are being distributed
    	int playersSidePits = turnID;
    	//number of stones we have to distribute
    	int stonesToDistribute = getNumStones(sidePitNum);
    	
    	//empty pit which player moved from
    	playerStates[turnID][sidePitNum] = 0;
    	
        for(int i = 0; i < stonesToDistribute; i++)
        {
        	//in previous move of stone we ended in kala,
        	//therefore another six stones have to be
        	//distributed over the opponent's pits
        	if(sidePitNum == 0)
        		playersSidePits = secondPlayer(playersSidePits);
        
        	//next pit to put a stone to
        	sidePitNum++;
        	
        	//we reached a kala
        	if(sidePitNum > NUM_SIDE_PITS)
        		sidePitNum = 0; //kala
        	
        	playerStates[playersSidePits][sidePitNum]++;
        	
        	//last moved stone
        	if(i == stonesToDistribute - 1)
        	{
        		//player did not end on their side
        		if(playersSidePits == secondPlayer())
        			changeTurn();
        		//player ended on their side
        		else
        		{
	        		//player did not end in their kala
	        		if(sidePitNum > 0)
	        		{
	        			//player ended in an empty pit and has capture
	        			if(playerStates[turnID][sidePitNum] == 1)
	        				capture(sidePitNum);
	        			
	        			//therefore the turn changes
	        			changeTurn();	
	        		}
        		}
        	}
        }
    }
    
    private void capture(int pit)
    {
    	int opposingPit = NUM_SIDE_PITS - (pit - 1);
    	
    	//put all to current player's kala
    	playerStates[turnID][0] += (getNumStones(pit) + getNumStones(secondPlayer(), opposingPit));
    	
    	//empty captured pits
    	playerStates[secondPlayer()][opposingPit] = 0;
    	playerStates[turnID][pit] = 0;
    }
    
    public KalaGameState clone() throws CloneNotSupportedException
    {
    	//deep copy this state
    	KalaGameState clonedState = (KalaGameState)super.clone();
    	
    	//deep copy playerStates array
    	clonedState.playerStates = playerStates.clone();
    	
    	//deep copy arrays in playerStates
    	for(int i = 0; i < playerStates.length; i++)
    		clonedState.playerStates[i] = playerStates[i].clone();
    	
    	return clonedState;
    }
}