/**
 * An exception representing an illegal move (i.e. an attempt
 * to select a side pit that has no stones in it)
 */ 
public class IllegalMoveException extends Exception
{
    /**
     *  Exception constructor.
     *  @param sidePitNum the side pit that resulted in the illegal move
     */
    public IllegalMoveException(int sidePitNum)
    {
        super("Side pit " + sidePitNum + " is empty");
    }   
}