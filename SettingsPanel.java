import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.*;
import javax.swing.border.*;

/**
 * Settings Panel
 */
public class SettingsPanel extends JPanel// implements MouseListener
{
	private JPanel textInputs;
	private JPanel radioButtons;
	private JButton startButton;
	
	private JTextField numStonesTF;
	private JTextField nameTF;
	private JRadioButton playerA;
	private JRadioButton playerB;
	private JRadioButton easy;
	private JRadioButton hard;
	
	private GameStatePanel gameStatePanel;
	
	private GridBagConstraints c = new GridBagConstraints();
	
	public SettingsPanel(GameStatePanel gameStatePanel)
	{
		setLayout(new GridBagLayout());
		setBorder(new CompoundBorder(new EmptyBorder(10, 10, 10, 10), new TitledBorder(null, "Game settings")));
		
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 1.0;
		c.insets = new Insets(0, 10, 10, 10);
		
		this.gameStatePanel = gameStatePanel;
		
		initializeContent();
	}
	
	private void initializeContent()
	{
		initializeTextInputs();
		initializeRadioButtons();
		initializeStartButton();
		
		revalidate();
	}
	
	private void initializeTextInputs()
	{
		final int TF_WIDTH = 10;
		
		textInputs = new JPanel();
		textInputs.setLayout(new GridLayout(4, 1));
		//textInputs.
		
		//text field for player's name
		JLabel nameLabel = (JLabel) new JLabel("Player's name");
		textInputs.add(nameLabel);
		nameTF = new JTextField(TF_WIDTH);
		textInputs.add(nameTF);
		
		//text field for starting number of stones
		JLabel numStonesLabel = new JLabel("Enter the number of stones to start with in each pit");
		textInputs.add(numStonesLabel);
		numStonesTF = new JTextField(TF_WIDTH);
		textInputs.add(numStonesTF);
		
		c.gridx = 0;
		c.gridy = 0;
		add(textInputs, c);
	}
	
	private void initializeRadioButtons()
	{
		radioButtons = new JPanel();
		radioButtons.setLayout(new GridLayout(1, 3, 10, 0));
		
		// --------------------- "order of play" panel -----------------------
		JPanel orderOfPlayPanel = new JPanel();
		orderOfPlayPanel.setLayout(new GridLayout(2, 1));
		orderOfPlayPanel.setBorder(new CompoundBorder(new TitledBorder(null, "Order of play"), new EmptyBorder(10, 10, 10, 10)));
		
		//"order of play" radio buttons
		playerA = new JRadioButton("Player A", true);
		playerB = new JRadioButton("Player B");
		
		//group "order of play" radio buttons
		ButtonGroup orderOfPlayGroup = new ButtonGroup();
		orderOfPlayGroup.add(playerA);
		orderOfPlayGroup.add(playerB);
		
		//add "order of play" radio buttons
		orderOfPlayPanel.add(playerA);
		orderOfPlayPanel.add(playerB);
		
		radioButtons.add(orderOfPlayPanel);
		
		// --------------------- "difficulty" panel -----------------------
		
		//"difficulty" panel
		JPanel difficultyPanel = new JPanel();
		difficultyPanel.setLayout(new GridLayout(2, 1));
		difficultyPanel.setBorder(new CompoundBorder(new TitledBorder(null, "Difficulty"), new EmptyBorder(10, 10, 10, 10)));
		
		//"difficulty" radio buttons
		easy = new JRadioButton("Easy", true);
		hard = new JRadioButton("Hard");
		
		//group "difficulty" radio buttons
		ButtonGroup difficultyGroup = new ButtonGroup();
		difficultyGroup.add(easy);
		difficultyGroup.add(hard);
		
		//add "difficulty" radio buttons
		difficultyPanel.add(easy);
		difficultyPanel.add(hard);
		
		radioButtons.add(difficultyPanel);
		
		c.gridx = 1;
		c.gridy = 0;
		add(radioButtons, c);
	}
	
	private void initializeStartButton()
	{
		startButton = new JButton("Start game");
		startButton.setMargin(new Insets(5, 10, 5, 10));
		startButton.addMouseListener(new StartGameListener(this, gameStatePanel));
		
		c.gridx = 0;
		c.gridy = 1;
		c.gridwidth = 2;
		c.fill = GridBagConstraints.CENTER;
		add(startButton, c);
	}
	
	public String getPlayerName() throws NoNameException
	{
		if(nameTF.getText().length() == 0)
			throw new NoNameException();
		else
			return nameTF.getText();
	}
	
	public int getNumStones() throws NoNumStonesException,
									 InvalidStartingStonesException,
									 NumberFormatException
	{
		if(numStonesTF.getText().length() == 0)
			throw new NoNumStonesException();
		
		int number = Integer.parseInt(numStonesTF.getText());
		
		if(number > 10 || number < 1)
			throw new InvalidStartingStonesException(number);
		
		return number;
	}
	
	public String getDifficulty()
	{
		return easy.isSelected() ? "easy" : "hard";
	}
	
	public String getSelectedOrder()
	{
		return playerA.isSelected() ? "A" : "B";
	}
}
