/**
 * An exception thrown when an illegal side pit was
 * specified (i.e. not in the range 1-6) for a move
 */
public class IllegalSidePitNumException extends RuntimeException
{
    /**
     *  Exception constructor.
     *  @param sidePitNum the illegal side pit that was selected.
     */
    public IllegalSidePitNumException(int sidePitNum)
    {
        super("No such side pit number: " + sidePitNum);
    }
}