
public class NoNameException extends Exception
{
	public NoNameException()
	{
		super("Player's name has been left empty!");
	}
}
