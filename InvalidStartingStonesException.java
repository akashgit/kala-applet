/**
 * An exception thrown when the number of stones
 * passed to the constructor of GameState is invalid (i.e. less than or equal to zero
 * or greater than 10).
 */
public class InvalidStartingStonesException extends RuntimeException
{
    /**
     *  Exception constructor.
     *  @param startingStones the illegal number of starting stones specified
     */ 
    public InvalidStartingStonesException(int startingStones)
    {
        super("The number of starting stones must be greater than 0 and less than or equal to 10 (attempted " + startingStones + ")");
    }
}