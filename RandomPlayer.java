/**
 * Computer player which chooses moves randomly 
 * @author Vaclav Hudec, Akash Srivastava
 */
public class RandomPlayer extends KalaPlayer
{	
	private final int NUM_SIDE_PITS = 6;
	
	public int chooseMove(KalaGameState gs) throws NoMoveAvailableException
	{
		//in case the current player cannot make any legal move
		if(gs.gameOver())
			throw new NoMoveAvailableException();
		
		int move;
		
		//choose a random move
		do
		{
			move = getRandomNumber(NUM_SIDE_PITS);
		}
		while(gs.getNumStones(move) == 0); //return only legal move
										   //do not cause the program to throw any exception on this matter
										   //i.e. exception thrown only on KayboardPlayer's (human) turn
		
		return move;
	}
	
	/**
	 * Generates a random number between 1 and a specified limit
	 * @param max Maximum number to be generated
	 * @return Generated number from 1 to max
	 */
	private static int getRandomNumber(int max)
	{
		//generate and return a random number from 1 to max
		return (int)Math.ceil(Math.random() * max);
	}
}