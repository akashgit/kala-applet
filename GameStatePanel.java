import java.awt.*;
import javax.swing.*;
import javax.swing.border.*;

public class GameStatePanel extends JPanel
{
	private final int NUM_PITS = 6;
	private final Dimension PIT_SIZE = new Dimension(100, 50);

	private KalaGame kalaGame;
	private KalaPlayer humanPlayer;
	private KalaPlayer firstPlayer;
	
	private JButton[] buttons;
	private JLabel[] labels;
	private JLabel computerLastMoveLabel;
	
	public GameStatePanel()
	{
		setLayout(new BorderLayout(10, 10));
		setBorder(new CompoundBorder(new EmptyBorder(10, 10, 10, 10), new EmptyBorder(10, 10, 10, 10)));
		
		buttons = new JButton[NUM_PITS];
		labels = new JLabel[NUM_PITS + 1]; //+ 1 kala
		
		computerLastMoveLabel = new JLabel();
	}
	
	public void createGame(KalaPlayer A, KalaPlayer B, int numStones)
	{
		kalaGame = new KalaGame(A, B, numStones);
		humanPlayer = A.getClass().toString().contains("class HumanPlayer") ? A : B;
		firstPlayer = A;
		
		for(int i = 0; i < NUM_PITS; i++)
		{
			buttons[i] = new JButton();
			labels[i] = new JLabel();
		}
		
		invalidateGameStatePanel(kalaGame.getState());
		
		if(humanPlayer != firstPlayer)
			kalaGame.play(this);
	}
	
	public void invalidateGameStatePanel(KalaGameState state)
	{
		removeAll();
		
		final GridLayout LAYOUT = new GridLayout(1, NUM_PITS, 10, 0);
		
		int humanID;
		int computerID;
		
		if(humanPlayer == firstPlayer)
		{
			humanID = 0;
			computerID = 1;
		}
		else
		{
			humanID = 1;
			computerID = 0;
		}
		
		//player A
		JPanel ASide = new JPanel();
		ASide.setLayout(LAYOUT);
		
		for(int i = NUM_PITS; i >= 1; i--)
		{
			if(humanID == 0)
				ASide.add(getNextButtonPit(state.getNumStones(humanID, i), i));
			else
				ASide.add(getNextLabelPit(state.getNumStones(computerID, i), i));
		}
		
		add(ASide, BorderLayout.NORTH);
		
		//player B
		JPanel BSide = new JPanel();
		BSide.setLayout(LAYOUT);
		
		for(int i = 1; i <= NUM_PITS; i++)
		{
			if(humanID == 1)
				BSide.add(getNextButtonPit(state.getNumStones(humanID, i), i));
			else
				BSide.add(getNextLabelPit(state.getNumStones(computerID, i), i));
		}
		
		add(BSide, BorderLayout.SOUTH);
			
		//kalas
		JLabel kalaA = new JLabel(""+state.getKala(0));
		JLabel kalaB = new JLabel(""+state.getKala(1));
		
		kalaA.setPreferredSize(new Dimension(20, 20));
		kalaB.setPreferredSize(new Dimension(20, 20));
		
		kalaA.setHorizontalAlignment(JLabel.CENTER);
		kalaA.setVerticalAlignment(JLabel.CENTER);
		kalaB.setHorizontalAlignment(JLabel.CENTER);
		kalaB.setVerticalAlignment(JLabel.CENTER);
		
		add(kalaA, BorderLayout.WEST);
		add(kalaB, BorderLayout.EAST);
		
		//last computer move
		getParent().add(computerLastMoveLabel, BorderLayout.SOUTH);
		
		revalidate();
	}
	
	private JButton getNextButtonPit(int stones, int id)
	{
		JButton pit = buttons[id-1];
		pit.setText(""+stones);
		pit.setPreferredSize(PIT_SIZE);	
		pit.removeMouseListener(pit.getMouseListeners()[0]);	
		pit.addMouseListener(new NextMoveListener((HumanPlayer)humanPlayer, kalaGame, this, id));		
		pit.setEnabled(stones > 0);
		
		return pit;
	}
	
	private JLabel getNextLabelPit(int stones, int id)
	{
		JLabel pit = labels[id-1];
		pit.setText(""+stones);
		pit.setPreferredSize(PIT_SIZE);
		pit.setHorizontalAlignment(JLabel.CENTER);
		pit.setVerticalAlignment(JLabel.CENTER);

		return pit;
	}
	
	public JLabel getLabel(int pitNum)
	{
		return labels[pitNum-1];
	}
	
	public void resetLabelColors()
	{
		for(int i = 0; i < NUM_PITS; i++)
		{
			labels[i].setForeground(Color.BLACK);
		}
	}
	
	public void setLastComputerMove(int move)
	{
		computerLastMoveLabel.setText("Last computer move: " + move);
	}
}
