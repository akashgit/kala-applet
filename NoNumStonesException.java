
public class NoNumStonesException extends Exception
{
	public NoNumStonesException()
	{
		super("You have to enter a number of stones to start with!");
	}
}
