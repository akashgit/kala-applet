import java.awt.event.*;

import javax.swing.JOptionPane;

/**
 * Starts a new game after "Start button" was pressed
 */
public class StartGameListener implements MouseListener
{
	private SettingsPanel settings;
	private GameStatePanel gameStatePanel;
	
	public StartGameListener(SettingsPanel settings, GameStatePanel gameStatePanel)
	{
		this.settings = settings;
		this.gameStatePanel = gameStatePanel;
	}
	
	public void mouseClicked(MouseEvent event)
	{
		String error = "";
		
		try
		{
			String difficulty = settings.getDifficulty();
			String order = settings.getSelectedOrder();
			String playerName = settings.getPlayerName();
			int numStones = settings.getNumStones();
			
			KalaPlayer A;
			KalaPlayer B;
			
			if(order == "A")
			{
				A = new HumanPlayer();
				B = difficulty == "easy" ? new RandomPlayer() : new ComputerPlayer();
			}
			else
			{
				B = new HumanPlayer();
				A = difficulty == "easy" ? new RandomPlayer() : new ComputerPlayer();
			}
				
			JOptionPane.showMessageDialog
			(
				gameStatePanel,
				"Difficulty: " + difficulty
					+ "\n\"" + playerName + "\" plays as player: " + order
					+ "\nNumber of stones: " + numStones,
				"Settings",
				JOptionPane.INFORMATION_MESSAGE
			);
			
			gameStatePanel.createGame(A, B, numStones);
		}
		catch(NoNumStonesException e)
		{
			error = e.getMessage();
		}
		catch(InvalidStartingStonesException e)
		{
			error = e.getMessage();
		}
		catch(NumberFormatException e)
		{
			error = "You have entered wrong number of stones! It must be an integer from 1 to 10";
		}
		catch(NoNameException e)
		{
			error = e.getMessage();
		}
		
		if(error.length() > 0)
		{
			JOptionPane.showMessageDialog(null, error, "Error", JOptionPane.ERROR_MESSAGE);
		}
	}
	
	public void mouseEntered(MouseEvent arg0)
	{}

	public void mouseExited(MouseEvent arg0)
	{}

	public void mousePressed(MouseEvent arg0)
	{}

	public void mouseReleased(MouseEvent arg0)
	{}
}
