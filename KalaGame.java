import javax.swing.JOptionPane;

/**
 * Kala game with two players and a state of the current game
 * @author Vaclav Hudec, Akash Srivastava
 */
public class KalaGame
{
	private KalaPlayer[] players;
	private KalaGameState state;

	/**
	 * Creates two players and an initial game state with a specified number of stones in each pit 
	 * @param playerA Player A
	 * @param playerB Player B
	 * @param initialStonesNum Initial number of stones in each pit
	 */
	public KalaGame(KalaPlayer playerA, KalaPlayer playerB, int initialStonesNum)
	{	
		final int NUM_PLAYERS = 2;
		
		//list of players in the game
		players = new KalaPlayer[NUM_PLAYERS];
		players[0] = playerA;
		players[1] = playerB;		
		
		//initial game state
		state = new KalaGameState(initialStonesNum);
	}
	
	/**
	 * Creates two players and an initial game state with 3 stones in each pit by default
	 * @param playerA Player A
	 * @param playerB Player B
	 */
	public KalaGame(KalaPlayer playerA, KalaPlayer playerB)
	{
		this(playerA, playerB, 3);
	}
	
	/**
	 * Handles the game playing. In a loop it asks users to insert a number of the pit 
	 * from which they want to make their move. When a player has no more moves to make, 
	 * it prints out the final score and the winner, and the game ends
	 */
	public void play(GameStatePanel statePanel)
	{
		//next turn
		if(!state.gameOver())
		{
			//whose turn is it?
			int currentPlayer = state.getTurn();
			int move = 0;
			
			statePanel.resetLabelColors();
			
			try
			{
				move = players[currentPlayer].chooseMove(state);
				state.makeMove(move);
			}
			catch(Exception e)
			{
				JOptionPane.showMessageDialog(null,	e.getMessage(),	"Settings",	JOptionPane.INFORMATION_MESSAGE);
			}
			
			boolean humanPlayer = players[state.getTurn()].getClass().toString().contains("class HumanPlayer");
			
			if(humanPlayer)
				statePanel.setLastComputerMove(move);
			
			statePanel.invalidateGameStatePanel(state);
			
			if(!humanPlayer || state.gameOver())
				play(statePanel);
		}
		else
		{
			JOptionPane.showMessageDialog
			(
				null,
				getPlayerLabel(0) + " (" + state.getScore(0) + ") : (" + state.getScore(1) + ") " + getPlayerLabel(1) + "\n\n" + getWinner() + " won!",
				"Congratulations!",
				JOptionPane.INFORMATION_MESSAGE
			);
		}
	}
	
	/**
	 * Determines a player's label
	 * @param playerID Player's ID
	 * @return A string representation of player's label
	 */
	private String getPlayerLabel(int playerID)
	{
		//get the player's label
		return playerID == 0 ? "Player A" : "Player B";
	}
	
	/**
	 * Determines the winner of the game by comparing both players' final score
	 * @return Label of the player who won
	 */
	private String getWinner()
	{
		//both players' score
		int score0 = state.getScore(0);
		int score1 = state.getScore(1);
		
		//compare scores and determine the winner
		if(score0 > score1)
			return getPlayerLabel(0);
		else if(score0 < score1)
			return getPlayerLabel(1);
		else
			return "Nobody";
	}
	
	public KalaGameState getState()
	{
		return state;
	}
	
	public KalaPlayer getPlayer(int id)
	{
		return players[id];
	}
}
