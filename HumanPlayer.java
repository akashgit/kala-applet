
public class HumanPlayer extends KalaPlayer
{
	private int move; 
	
	public int chooseMove(KalaGameState gs) throws NoMoveAvailableException
	{
		return move;
	}

	public void setMove(int move)
	{
		this.move = move;
	}
}
