import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JButton;
import javax.swing.JOptionPane;

public class NextMoveListener implements MouseListener
{
	private HumanPlayer humanPlayer;
	private KalaGame kalaGame;
	private GameStatePanel gameStatePanel;
	private int buttonID;
	
	public NextMoveListener(HumanPlayer humanPlayer, KalaGame kalaGame, GameStatePanel gameStatePanel, int buttonID)
	{
		this.humanPlayer = humanPlayer;
		this.kalaGame = kalaGame;
		this.gameStatePanel = gameStatePanel;
		this.buttonID = buttonID;
	}

	public void mouseClicked(MouseEvent event)
	{
		int move = buttonID;
		
		humanPlayer.setMove(move);
		kalaGame.play(gameStatePanel);
	}

	public void mouseEntered(MouseEvent arg0)
	{}

	public void mouseExited(MouseEvent arg0)
	{}

	public void mousePressed(MouseEvent arg0)
	{}

	public void mouseReleased(MouseEvent arg0)
	{}
	
	
}
