import java.awt.*;
import javax.swing.*;
import javax.swing.border.*;

/**
 * Kala applet
 */
public class KalaApplet extends JApplet
{
	private SettingsPanel settings;
	private GameStatePanel gameStatePanel;
	
	public void init()
	{
		setSize(600, 500);
		setMinimumSize(getSize());
		setLayout(new BorderLayout(0, 0));
		
		initializeContents();
		
		setVisible(true);
	}
	
	public void initializeContents()
	{
		gameStatePanel = new GameStatePanel();
		settings = new SettingsPanel(gameStatePanel);
		
		add(settings, BorderLayout.PAGE_START);
		add(gameStatePanel, BorderLayout.CENTER);
	}
	
	public SettingsPanel getSettings()
	{
		return settings;
	}
	
	public GameStatePanel getGameStatePanel()
	{
		return gameStatePanel;
	}
}
