/**
 * An exception thrown when an illegal player number was
 * specified to a method (i.e. not 0 or 1 - Player A and B 
 * respectively) 
 */
public class IllegalPlayerNumException extends RuntimeException
{
	/**
     *  Exception constructor.
     *  @param playerNum the illegal player number that triggered the exception
     */ 
    public IllegalPlayerNumException(int playerNum)
    {
        super("No such player number: " + playerNum);
    }
}