/**
 * Computer player; enters the tournament
 * @author Vaclav Hudec, Akash Srivastava
 */
public class ComputerPlayer extends KalaPlayer
{
	private int turn;
	
	private final int DEPTH_LIMIT = 6;
	private final int SIDE_PITS_NUM = 6;
	
	public int chooseMove(KalaGameState state)
	{
		//player who has to make a move
		turn = state.getTurn();
		
		//evaluate moves
		int chosenMove = minimax(state, DEPTH_LIMIT);
		
		//final move
		return chosenMove;
	}
	
	private int minimax(KalaGameState state, int finalDepth)
	{
		//move to return when back in the initial depth
		int bestMove;
		
		//depth has been reached
		if(finalDepth <= 0)
		{
			return evaluate(state);
		}
		else if(state.getTurn() == turn)
		{
			int move = bestMove = 1;
			int maxValue = -1200;
			
			while(move <= SIDE_PITS_NUM)
			{
				try
				{
					//copy current state
					KalaGameState stateCopy = new KalaGameState(1);
					try{stateCopy = state.clone();}
					catch(Exception e){}
			
					//try this move
					stateCopy.makeMove(move);
					
					//evaluate this move
					int value = minimax(stateCopy, finalDepth-1);
					
					//get the highest value so far
					if(maxValue <= value)
					{
						maxValue = value;
						bestMove = move;
					}
				}
				//if a move should cause a problem (like a move from an empty pit),
				//ignore it and check the next move (in 'finally' block) 
				catch(IllegalMoveException e){}
				finally
				{
					move++;
				}
			}
			
			//if the search tree has reached the initial level of depth search,
			//return the best move,
			//otherwise return the value of the move
			return finalDepth == DEPTH_LIMIT ? bestMove : maxValue;
		}
		else
		{
			int move = bestMove = 1;
			int minValue = 1200;
			
			while(move <= SIDE_PITS_NUM)
			{
				try
				{
					//copy current state
					KalaGameState stateCopy = new KalaGameState(1);
					try{stateCopy = state.clone();}
					catch(Exception e){}
					
					//try this move
					stateCopy.makeMove(move);
					
					//evaluate this move
					int value = minimax(stateCopy, finalDepth-1);
					
					//get the lowest value so far
					if(minValue >= value)
					{
						minValue = value;
						bestMove = move;
					}
				}
				//if a move should cause a problem (like a move from an empty pit),
				//ignore it and check the next move (in 'finally' block) 
				catch(IllegalMoveException e){}
				finally
				{
					move++;
				}
			}
			
			//if the search tree has reached the initial level of depth search,
			//return the best move,
			//otherwise return the value of the move
			return finalDepth == DEPTH_LIMIT ? bestMove : minValue;
		}
	}
	
	private int evaluate(KalaGameState state)
	{
		int score0 = state.getScore(0);
		int score1 = state.getScore(1);
		
		//evaluation is done for the player who has to make a move
		return turn == 0 ? (score0 - score1) : (score1 - score0);
	}
}
