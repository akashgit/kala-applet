/**
 *  An exception thrown by a player when there are no moves 
 *  available to choose from (i.e. the game should be over)
 */
public class NoMoveAvailableException extends Exception
{}